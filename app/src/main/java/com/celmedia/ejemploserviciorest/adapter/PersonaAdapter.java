package com.celmedia.ejemploserviciorest.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.celmedia.ejemploserviciorest.R;
import com.celmedia.ejemploserviciorest.entidades.Persona;

import java.util.List;

/**
 * Created by harce on 13-11-2015.
 */
public class PersonaAdapter extends BaseAdapter {
    private List<Persona> dataSet;
    private Context context;

    public PersonaAdapter(Context context, List<Persona> dataSet) {
        this.context = context;
        this.dataSet = dataSet;
    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public Object getItem(int position) {
        return dataSet.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderItem viewHolder;


        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item, parent, false);

            viewHolder = new ViewHolderItem();
            viewHolder.txtId = (TextView) convertView.findViewById(R.id.txtId);
            viewHolder.txtNombre= (TextView) convertView.findViewById(R.id.txtNombre);
            viewHolder.txtApellido= (TextView) convertView.findViewById(R.id.txtApellido);
            viewHolder.txtRut = (TextView) convertView.findViewById(R.id.txtRut);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolderItem)convertView.getTag();
        }

        Persona persona = (Persona)getItem(position);
        if(persona != null){
            viewHolder.txtId.setText(persona.getId());
            viewHolder.txtNombre.setText(persona.getNombre());
            viewHolder.txtApellido.setText(persona.getApellido());
            viewHolder.txtRut.setText(persona.getRut());
        }

        return convertView;
    }

    static class ViewHolderItem {
        TextView txtId;
        TextView txtNombre;
        TextView txtApellido;
        TextView txtRut;
    }

}
