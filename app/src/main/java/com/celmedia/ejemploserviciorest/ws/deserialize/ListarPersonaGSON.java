package com.celmedia.ejemploserviciorest.ws.deserialize;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.celmedia.ejemploserviciorest.entidades.Persona;
import com.celmedia.ejemploserviciorest.entidades.RespuestaGet;
import com.celmedia.ejemploserviciorest.entidades.RespuestaPost;
import com.celmedia.ejemploserviciorest.ws.WSEjemplo;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by harce on 05-10-2015.
 */
public class ListarPersonaGSON implements JsonDeserializer<RespuestaGet> {

    @Override
    public RespuestaGet deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        RespuestaGet response = new RespuestaGet();
        List<Persona> values = new ArrayList<>();
        JsonArray jsonArray = json.getAsJsonArray();
        try {
            for(JsonElement aux : jsonArray){
                JsonObject jsonObject = aux.getAsJsonObject();
                String id = jsonObject.get("id").getAsString();
                String nombre = jsonObject.get("nombre").getAsString();
                String apellido = jsonObject.get("apellido").getAsString();
                String rut = jsonObject.get("rut").getAsString();
                Persona personaAux = new Persona();
                personaAux.setId(id);
                personaAux.setNombre(nombre);
                personaAux.setApellido(apellido);
                personaAux.setRut(rut);
                values.add(personaAux);
            }

            response.setValues(values);
        } catch (Exception e) {
            response = null;
        }
        return response;
    }


}
