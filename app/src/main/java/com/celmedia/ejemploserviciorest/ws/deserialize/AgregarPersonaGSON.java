package com.celmedia.ejemploserviciorest.ws.deserialize;

import com.celmedia.ejemploserviciorest.entidades.RespuestaPost;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by harce on 05-10-2015.
 */
public class AgregarPersonaGSON implements JsonDeserializer<RespuestaPost> {

    @Override
    public RespuestaPost deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        RespuestaPost response = new RespuestaPost();
        JsonObject jsonObject = json.getAsJsonObject();
        try {
            String respuesta = jsonObject.get("status").getAsString();

            response.setRespuesta(respuesta);
        } catch (Exception e) {
            response = null;
        }
        return response;
    }
}
