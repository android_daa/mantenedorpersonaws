package com.celmedia.ejemploserviciorest.entidades;

/**
 * Created by harce on 13-11-2015.
 */
public class RespuestaPost {
    private String respuesta;

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }
}
