package com.celmedia.ejemploserviciorest.entidades;

import java.util.List;

/**
 * Created by harce on 13-11-2015.
 */
public class RespuestaGet {
    private List<Persona> values;

    public List<Persona> getValues() {
        return values;
    }

    public void setValues(List<Persona> values) {
        this.values = values;
    }
}
