package com.celmedia.ejemploserviciorest;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.celmedia.ejemploserviciorest.entidades.Persona;
import com.celmedia.ejemploserviciorest.entidades.RespuestaGet;
import com.celmedia.ejemploserviciorest.entidades.RespuestaPost;
import com.celmedia.ejemploserviciorest.ws.WSEjemplo;

public class AgregarPersonaActivity extends AppCompatActivity {

    private EditText txtId;
    private EditText txtNombre;
    private EditText txtApellido;
    private EditText txtRut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agregar_persona);
        txtId = (EditText)findViewById(R.id.txtIdManual);
        txtNombre = (EditText)findViewById(R.id.txtNombre);
        txtApellido = (EditText)findViewById(R.id.txtApellido);
        txtRut = (EditText)findViewById(R.id.txtRut);


    }

    public void onClickAgregarPersona(View v){
        Persona p = new Persona();
        p.setRut(txtRut.getText().toString());
        p.setApellido(txtApellido.getText().toString());
        p.setNombre(txtNombre.getText().toString());
        p.setId(txtId.getText().toString());
        new EnviaPersonasTask(this, true).execute(p);
    }

    public void onClickVolver(View v){
        finish();
    }

    private class EnviaPersonasTask extends AsyncTask<Persona, Void, RespuestaPost> {

        ProgressDialog mProgress;
        boolean muestraProgress;
        Context context;

        public EnviaPersonasTask(Context context, boolean muestraProgress) {
            this.context = context;
            this.muestraProgress = muestraProgress;
        }

        @Override
        protected void onPreExecute() {
            try {
                mProgress = new ProgressDialog(context);
                mProgress.setIndeterminate(true);
                mProgress.setMessage("Enviando Información");
                mProgress.setCancelable(false);

                if (muestraProgress) {
                    mProgress.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected RespuestaPost doInBackground(Persona... values) {
            RespuestaPost aux;
            try {
                aux = WSEjemplo.
                        getInstance().registroPersona(context, values[0]);

                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                }
            } catch (Exception e) {
                aux = null;
            }

            return aux;
        }

        @Override
        protected void onPostExecute(RespuestaPost values) {
            if (values != null) {
                try {
                    muestraRespuesta(values.getRespuesta());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (muestraProgress) {
                mProgress.dismiss();
            }
        }
    }

    private void muestraRespuesta(String respuesta) {
        Toast.makeText(this, respuesta, Toast.LENGTH_LONG).show();
    }
}
