package com.celmedia.ejemploserviciorest;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.celmedia.ejemploserviciorest.adapter.PersonaAdapter;
import com.celmedia.ejemploserviciorest.entidades.Persona;
import com.celmedia.ejemploserviciorest.entidades.RespuestaGet;
import com.celmedia.ejemploserviciorest.ws.WSEjemplo;

import java.util.ArrayList;
import java.util.List;


public class VerDatosActivity extends AppCompatActivity {

    private ListView ls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_datos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView txtSalir = (TextView)toolbar.findViewById(R.id.txtVolver);
        txtSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ImageView imageViewLogo = (ImageView)toolbar.findViewById(R.id.imageViewLogo);
        imageViewLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new ObtieneListaPersonasTask(VerDatosActivity.this, true).execute();
            }
        });


        ls = (ListView) findViewById(R.id.listView);
        new ObtieneListaPersonasTask(this, true).execute();
        //ls.setAdapter(new PersonaAdapter(this, getValues()));
    }


    private List<Persona> getValues() {
        List<Persona> values = new ArrayList<>();
        for (int i = 1; i < 30; i++) {
            Persona aux = new Persona();
            aux.setId(i + "");
            aux.setNombre("Nombre " + i);
            aux.setNombre("Apellido " + i);
            aux.setNombre(i + "-9");
            values.add(aux);
        }
        return values;
    }

    private void cargaPersonas(List<Persona> values) {
        ls.setAdapter(new PersonaAdapter(this, values));
    }


    private class ObtieneListaPersonasTask extends AsyncTask<Void, Void, RespuestaGet> {

        ProgressDialog mProgress;
        boolean muestraProgress;
        Context context;

        public ObtieneListaPersonasTask(Context context, boolean muestraProgress) {
            this.context = context;
            this.muestraProgress = muestraProgress;
        }

        @Override
        protected void onPreExecute() {
            try {
                mProgress = new ProgressDialog(context);
                mProgress.setIndeterminate(true);
                mProgress.setMessage("Obteniendo Información");
                mProgress.setCancelable(false);

                if (muestraProgress) {
                    mProgress.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected RespuestaGet doInBackground(Void... values) {
            RespuestaGet aux;
            try {
                aux = WSEjemplo.
                        getInstance().listarPersonas(context);

                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                }
            } catch (Exception e) {
                aux = null;
            }

            return aux;
        }

        @Override
        protected void onPostExecute(RespuestaGet values) {
            if (values != null) {
                try {
                   cargaPersonas(values.getValues());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (muestraProgress) {
                mProgress.dismiss();
            }
        }
    }



}
