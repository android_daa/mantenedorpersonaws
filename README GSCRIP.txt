function myFunction() {
  var SPREADSHEET_ID = "17fXV9uSfSu97zhh-z2dx7orr_ZmdCiBB1G7mbikisvE";   //ID de la hoja de Nomina.
  var HEADER_SIZE = 2;  //Las 2 filas antes de los datos a guardar
  var FOOTER_SIZE = 0;  
  
  var sheet = SpreadsheetApp.openById(SPREADSHEET_ID);               //Guardamos la SpreadSheet como variable
  var data  = sheet.getDataRange().getValues();                      //Obtenemos todos los datos de la SS
  var total =(data.length)-(HEADER_SIZE+FOOTER_SIZE);   
  
  var sl =  HEADER_SIZE;                                             //Es el limite superior del SS, posicion donde termina el Header
  var il =  (total+HEADER_SIZE);                                     //Es el limite inferior del SS, posicion donde comienza el Header
  var datos = [];//var users = []; 
  
  for(i=sl; i<il; i++){
    datos.push(data[i]);                                            //Los limites nos permiten solo obtener los valores deseados, vamos
  }    
  
   Logger.log(datos);
   
   var datosAuxData = [];
   
   for(i in datos){
 
    var row           = datos[i];
    var id        = row[0];
    var nombre = row[1];
    var apellido = row[2];
    var rut = row[3];
    
 
    var personaAux= new toObject(id,nombre, apellido, rut); //creamos un objeto, con cada usuario
    Logger.log(personaAux);
    datosAuxData[i] = personaAux;
  }
  return datosAuxData;
   
}

function toObject(id, nombre, apellido, rut){
  this.id = id;
  this.nombre = nombre;
  this.apellido = apellido;
  this.rut = rut;
}

function doGet() {
  var datos = myFunction();
  return ContentService.createTextOutput(JSON.stringify(datos)).setMimeType(ContentService.MimeType.JSON); //convertimos la respuesta a JSON
  //return ContentService.createTextOutput("Hola Mundo");
}

function doPost(e){
   //id: 1JmY1A0S97r0YHJCXYR2WaciPZohj7vRASFMGKvhC17U
  var SPREADSHEET_ID = "17fXV9uSfSu97zhh-z2dx7orr_ZmdCiBB1G7mbikisvE";   //ID de la hoja de Nomina.  
  var ss = SpreadsheetApp.openById(SPREADSHEET_ID);               //Guardamos la SpreadSheet como variable
  var sheet = ss.getSheets()[0];   
  
  var lastRow = sheet.getLastRow(); // Determine the last row in the Spreadsheet that contains any values
  var cell = sheet.getRange('a1').offset(lastRow, 0);
  
  var record = '{"status": "OK"}';
  
  if (e != null){
    if(e.parameter.id != null && e.parameter.nombre != null && e.parameter.apellido != null && e.parameter.rut != null){
      var idParam = e.parameter.id;
      var nombre = e.parameter.nombre;
      var apellido = e.parameter.apellido;
      var rut = e.parameter.rut;
      var bugCount = sheet.getRange("G1").getValue();
      bugCount++;
      
      cell.setValue(idParam); // Set the value of the cell to userName
      cell.offset(0, 1).setValue(nombre); // Set the value of the adjacent cell to age
      cell.offset(0, 2).setValue(apellido);
      cell.offset(0,3).setValue(rut);
      cell.offset(0,4).setValue(bugCount);
      sheet.getRange("G1").setValue(bugCount);
    }else{
      record = '{"status": Error, faltan parametros"}';
    }
  }else{
    record = '{"status": Error, faltan los parametros"}';
  }  
  
  return ContentService.createTextOutput(record);
}